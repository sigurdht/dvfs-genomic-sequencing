# DVFS for genomic sequencing

## Report

[Report.pdf](Report.pdf) is the project report. It's source code is located in the directory [`report/`](report/).

### Compiling report

Requires docker and docker-compose.

Compile once:

```sh
docker-compose up plot && docker-compose up report
```

Automatically recompile on changes:

```sh
docker-compose up
```
