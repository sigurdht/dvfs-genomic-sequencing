#!/usr/bin/python3

import inspect
import os
import subprocess
import time
import plac

from plot_dvfs_theory import plot_dvfs_theory
from plot_cpu_frequency_test_results import plot_cpu_frequency_test_results
from plot_dataset_distribution import plot_dataset_distribution

def plot_all(
    output_directory=os.path.join(os.path.dirname(__file__), "../../report/figures/plots/")
):
    plot_dvfs_theory(output_dir=output_directory)
    plot_cpu_frequency_test_results(output_dir=output_directory)
    plot_dataset_distribution(output_dir=output_directory)

if __name__ == "__main__":
    plac.call(plot_all)
