#!/usr/bin/python3

import inspect
import os
import subprocess
import time
import plac
import matplotlib.pyplot as plt
import numpy as np

def plot_dataset_distribution_1():
    dataset = []
    try:
        with open(os.path.join(os.path.dirname(__file__), "hs37d5_single_P1_ST_200924.txt"), "r") as file:
            for line in file.readlines():
                try:
                    data = float(line)
                except ValueError:
                    continue
                dataset.append(data)
    except FileNotFoundError:
        print("hs37d5_single_P1_ST_200924.txt not found, skipping plot_dataset_distribution_1")
        return "dont save"
    print("len(dataset):", len(dataset))
    print("average:", np.average(np.array(dataset)))
    print("mean:", np.mean(np.array(dataset)))
    print("std:", np.std(np.array(dataset)))
    def create_average_buckets(bucket_size):
        return [
            int(np.average(np.array(dataset[bucket_size*i:bucket_size*(i+1)])))
            for i in range(len(dataset)//bucket_size)
        ]
    average_buckets_500_000 = create_average_buckets(500_000)
    average_buckets_500_000_x_axis = np.linspace(0, 4_000_000 - 500_000, len(average_buckets_500_000))
    average_buckets_10_000 = create_average_buckets(10_000)
    average_buckets_10_000_x_axis = np.linspace(0, 4_000_000 - 10_000, len(average_buckets_10_000))
    average_buckets_500_000_plot = []
    for value in average_buckets_500_000:
        average_buckets_500_000_plot.append(value)
        average_buckets_500_000_plot.append(value)
    average_buckets_500_000_plot_x_axis = []
    for x_start in average_buckets_500_000_x_axis:
        average_buckets_500_000_plot_x_axis.append(x_start)
        average_buckets_500_000_plot_x_axis.append(x_start+500_000-1)
    plt.xlabel("Short read number")
    plt.ylabel("Time[μs]")
    # plt.bar(average_buckets_500_000_x_axis, average_buckets_500_000, width=500_000, label="500_000 per bucket", align="edge", color=["tab:blue", "tab:orange"])
    plt.plot(average_buckets_10_000_x_axis, average_buckets_10_000, label="10_000 per bucket")
    plt.plot(average_buckets_500_000_plot_x_axis, average_buckets_500_000_plot, label="500_000 per bucket")
    # plt.ylim(0, 2500)
    plt.legend()


def plot_dataset_distribution(output_dir="./"):
    for function, filename in [
        (plot_dataset_distribution_1, "dataset_distribution.svg",),
    ]:
        plt.clf()
        return_value = function()
        if return_value is "dont save":
            continue
        plt.savefig(os.path.join(output_dir, filename))


if __name__ == "__main__":
    plac.call(plot_dataset_distribution)
