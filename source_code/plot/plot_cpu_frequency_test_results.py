#!/usr/bin/python3

import inspect
import os
import subprocess
import sys
import time
import plac
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np

sys.path.append(os.path.join(os.path.dirname(__file__), "../swaram_dvfs_odroid"))
from power_data import read_bwa_dvfs_csv_entries_from_file as read_results

def plot_experiment_1_comparison():
    results = read_results(os.path.join(os.path.dirname(__file__), "test_results.csv"))
    results_0_to_3 = sorted(
        filter(lambda result: result.cpu_4_to_7_frequency_kilohertz == 2_000_000, results),
        key=lambda result: result.cpu_0_to_3_frequency_kilohertz,
    )
    results_4_to_7 = sorted(
        filter(lambda result: result.cpu_0_to_3_frequency_kilohertz == 1_500_000, results),
        key=lambda result: result.cpu_4_to_7_frequency_kilohertz,
    )
    frequency_0_to_3 = np.array([result.cpu_0_to_3_frequency_kilohertz for result in results_0_to_3]) / 1e6
    frequency_4_to_7 = np.array([result.cpu_4_to_7_frequency_kilohertz for result in results_4_to_7]) / 1e6
    energy_0_to_3 = np.array([result.total_energy_consumption_watthours for result in results_0_to_3])
    energy_4_to_7 = np.array([result.total_energy_consumption_watthours for result in results_4_to_7])
    time_0_to_3 = np.array([result.time_usage_seconds for result in results_0_to_3])
    time_4_to_7 = np.array([result.time_usage_seconds for result in results_4_to_7])
    if False:
        # Print metrics
        f_min_0_to_3 = np.argmin(energy_0_to_3)
        f_min_4_to_7 = np.argmin(energy_4_to_7)
        f_max_0_to_3 = -1
        f_max_4_to_7 = -1
        print("A7 min frequency:", frequency_0_to_3[f_min_0_to_3], "GHz")
        print("A7 max frequency:", frequency_0_to_3[f_max_0_to_3], "GHz")
        print("A15 min frequency:", frequency_4_to_7[f_min_4_to_7], "GHz")
        print("A15 max frequency:", frequency_4_to_7[f_max_4_to_7], "GHz")
        print("A7 min energy:", energy_0_to_3[f_min_0_to_3])
        print("A7 max energy:", energy_0_to_3[f_max_0_to_3])
        print("A15 min energy:", energy_4_to_7[f_min_4_to_7])
        print("A15 max energy:", energy_4_to_7[f_max_4_to_7])
        print("A7 difference energy:", energy_0_to_3[f_max_0_to_3] - energy_0_to_3[f_min_0_to_3])
        print("A7 relative energy:", ((energy_0_to_3[f_max_0_to_3] - energy_0_to_3[f_min_0_to_3]) / energy_0_to_3[f_max_0_to_3]) * 100, "%")
        print("A15 difference energy:", energy_4_to_7[f_max_4_to_7] - energy_4_to_7[f_min_4_to_7])
        print("A15 relative energy:", ((energy_4_to_7[f_max_4_to_7] - energy_4_to_7[f_min_4_to_7]) / energy_4_to_7[f_max_4_to_7]) * 100, "%")
        print("A7 min time:", time_0_to_3[f_min_0_to_3])
        print("A7 max time:", time_0_to_3[f_max_0_to_3])
        print("A15 min time:", time_4_to_7[f_min_4_to_7])
        print("A15 max time:", time_4_to_7[f_max_4_to_7])
        print("A7 difference time:", time_0_to_3[f_min_0_to_3] - time_0_to_3[f_max_0_to_3])
        print("A7 relative time:", ((time_0_to_3[f_min_0_to_3] - time_0_to_3[f_max_0_to_3]) / time_0_to_3[f_max_0_to_3]) * 100, "%")
        print("A15 difference time:", time_4_to_7[f_min_4_to_7] - time_4_to_7[f_max_4_to_7])
        print("A15 relative time:", ((time_4_to_7[f_min_4_to_7] - time_4_to_7[f_max_4_to_7]) / time_4_to_7[f_max_4_to_7]) * 100, "%")
        exit(1)
    xlabel = "frequency[GHz]"
    energy_axis_label = "E_total[Wh]"
    time_axis_label = "T_total[s]"
    _, (energy_axis, time_axis) = plt.subplots(nrows=2, ncols=1, figsize=(6, 8))
    energy_axis.set_xlabel(xlabel)
    energy_axis.set_ylabel(energy_axis_label)
    time_axis.set_xlabel(xlabel)
    time_axis.set_ylabel(time_axis_label)
    plots = [
        energy_axis.plot(frequency_0_to_3, energy_0_to_3, label="A7 energy", color="tab:red", marker="o"),
        energy_axis.plot(frequency_4_to_7, energy_4_to_7, label="A15 energy", color="tab:blue", marker="o"),
        time_axis.plot(frequency_0_to_3, time_0_to_3, label="A7 time", color="tab:green", marker="o"),
        time_axis.plot(frequency_4_to_7, time_4_to_7, label="A15 time", color="tab:orange", marker="o"),
    ]
    # plt.legend(handles=[
    #     Line2D([0], [0], color=plot[0].get_color(), label=plot[0].get_label())
    #     for plot in plots
    # ])
    energy_axis.legend()
    time_axis.legend()

def plot_time_usage_of_frequency():
    results = read_results(os.path.join(os.path.dirname(__file__), "test_results.csv"))
    results_0_to_3 = sorted(
        filter(lambda result: result.cpu_4_to_7_frequency_kilohertz == 2_000_000, results),
        key=lambda result: result.cpu_0_to_3_frequency_kilohertz,
    )
    results_4_to_7 = sorted(
        filter(lambda result: result.cpu_0_to_3_frequency_kilohertz == 1_500_000, results),
        key=lambda result: result.cpu_4_to_7_frequency_kilohertz,
    )
    frequency_0_to_3 = np.array([result.cpu_0_to_3_frequency_kilohertz for result in results_0_to_3]) / 1e6
    frequency_4_to_7 = np.array([result.cpu_4_to_7_frequency_kilohertz for result in results_4_to_7]) / 1e6
    time_0_to_3 = np.array([result.time_usage_seconds for result in results_0_to_3])
    time_4_to_7 = np.array([result.time_usage_seconds for result in results_4_to_7])
    plt.xlabel("frequency[GHz]")
    plt.ylabel("T[s]")
    plt.plot(frequency_0_to_3, time_0_to_3, label="CPU 0 to 3")
    plt.plot(frequency_4_to_7, time_4_to_7, label="CPU 4 to 7")
    plt.legend()

def get_results(csv_file_path, vfi):
    results = read_results(os.path.join(os.path.dirname(__file__), csv_file_path))
    match vfi:
        case "cpu_0_to_3":
            filter_function = lambda result: result.cpu_4_to_7_frequency_kilohertz == 2_000_000
        case "cpu_4_to_7":
            filter_function = lambda result: result.cpu_0_to_3_frequency_kilohertz == 1_500_000
    results = filter(filter_function, results)
    results = sorted(
        results,
        key=lambda result: getattr(result, f"{vfi}_frequency_kilohertz"),
    )
    return results


def plot_cpu_time_and_energy(csv_file_path="test_results.csv", plot_energy=True, plot_time=True, use_twinx=True, plot_type="plot", plot_marker="o", plot_average=False, average_plot_type="plot", average_plot_marker="", average_same_color=False, vfi="cpu_4_to_7", per_short_read=True, legend_location="best"):
    results = get_results(csv_file_path, vfi)
    number_of_short_reads = 4e6
    frequency = np.array([getattr(result, f"{vfi}_frequency_kilohertz") for result in results]) / 1e6
    energy = np.array([result.total_energy_consumption_watthours for result in results])
    time = np.array([result.time_usage_seconds for result in results])
    xlabel = "frequency[GHz]"
    energy_axis_label = "E_total[Wh]"
    time_axis_label = "T_total[s]"

    if per_short_read:
        energy *= 1e6 / number_of_short_reads
        time *= 1e6 / number_of_short_reads
        energy_axis_label = "E_total per short read[μWh]"
        time_axis_label = "time per short read[μs]"
    
    average_frequency = np.array(sorted(list(set(frequency))))
    average_energy = np.array([
        np.average(energy[np.where(frequency == f)[0]])
        for f in average_frequency
    ])
    average_time = np.array([
        np.average(time[np.where(frequency == f)[0]])
        for f in average_frequency
    ])

    if False:
        # Print metrics
        f_average_min = np.argmin(average_energy)
        f_average_max = -1
        print("min frequency:", average_frequency[f_average_min], "GHz")
        print("max frequency:", average_frequency[f_average_max], "GHz")
        print("min energy:", average_energy[f_average_min])
        print("max energy:", average_energy[f_average_max])
        print("difference energy:", average_energy[f_average_max] - average_energy[f_average_min])
        print("relative energy:", ((average_energy[f_average_max] - average_energy[f_average_min]) / average_energy[f_average_max]) * 100, "%")
        print("min time:", average_time[f_average_min])
        print("max time:", average_time[f_average_max])
        print("difference time:", average_time[f_average_min] - average_time[f_average_max])
        print("relative time:", ((average_time[f_average_min] - average_time[f_average_max]) / average_time[f_average_max]) * 100, "%")
        exit(1)

    if False:
        # Print data for optimize_cpu_frequency.py
        average_frequency *= 1e6
        if per_short_read:
            average_energy /= 1e6
            average_time /= 1e6
        else:
            average_energy *= number_of_short_reads
            average_time *= number_of_short_reads
        print("CPU_4_TO_7_AVAILABLE_FREQUENCIES =", list(average_frequency))
        print("CPU_4_TO_7_REFERENCE_ENERGY_PER_SHORT_READ =", list(average_energy))
        print("CPU_4_TO_7_REFERENCE_TIME_PER_SHORT_READ =", list(average_time))
        # Exit status 1 to stop eventual &&-chained command in shell script
        exit(1)
    
    if not use_twinx and plot_energy and plot_time:
        _, (energy_axis, time_axis) = plt.subplots(nrows=2, ncols=1, figsize=(6, 8))
    else:
        _, left_axis = plt.subplots()
        if plot_energy:
            energy_axis = left_axis
            if plot_time:
                time_axis = energy_axis.twinx()
        else:
            time_axis = left_axis

    if plot_energy:
        energy_axis.set_xlabel(xlabel)
        energy_axis.set_ylabel(energy_axis_label)
    if plot_time:
        time_axis.set_xlabel(xlabel)
        time_axis.set_ylabel(time_axis_label)

    match vfi:
        case "cpu_0_to_3":
            energy_color, time_color = "tab:red", "tab:green"
            average_energy_color, average_time_color = "tab:blue", "tab:orange"
            energy_plot_label = "A7 energy"
            average_energy_plot_label = "A7 average energy"
            time_plot_label = "A7 time"
            average_time_plot_label = "A7 average time"
        case "cpu_4_to_7":
            energy_color, time_color = "tab:blue", "tab:orange"
            average_energy_color, average_time_color = "tab:red", "tab:green"
            energy_plot_label = "A15 energy"
            average_energy_plot_label = "A15 average energy"
            time_plot_label = "A15 time"
            average_time_plot_label = "A15 average time"

    if average_same_color:
        average_energy_color, average_time_color = energy_color, time_color

    plots = []
    if plot_energy:
        match plot_type:
            case "plot":
                plots.append(energy_axis.plot(frequency, energy, label=energy_plot_label, color=energy_color, marker=plot_marker))
            case "scatter":
                plots.append(energy_axis.plot(frequency, energy, label=energy_plot_label, color=energy_color, lw=0, marker="o"))
        if plot_average:
            print("avg plot type:", average_plot_type)
            match average_plot_type:
                case "plot":
                    plots.append(energy_axis.plot(average_frequency, average_energy, label=average_energy_plot_label, color=average_energy_color, marker=average_plot_marker))
                case "scatter":
                    plots.append(energy_axis.plot(average_frequency, average_energy, label=average_energy_plot_label, color=average_energy_color, lw=0, marker="o"))
    if plot_time:
        match plot_type:
            case "plot":
                plots.append(time_axis.plot(frequency, time, label=time_plot_label, color=time_color, marker=plot_marker))
            case "scatter":
                plots.append(time_axis.plot(frequency, time, label=time_plot_label, color=time_color, lw=0, marker="o"))
        if plot_average:
            match average_plot_type:
                case "plot":
                    plots.append(time_axis.plot(average_frequency, average_time, label=average_time_plot_label, color=average_time_color, marker=average_plot_marker))
                case "scatter":
                    plots.append(time_axis.plot(average_frequency, average_time, label=average_time_plot_label, color=average_time_color, lw=0, marker="o"))

    if use_twinx:
        legend_handles = [
            Line2D([0], [0], marker=plot[0].get_marker(), color=plot[0].get_color(), label=plot[0].get_label())
            for plot in plots
        ]
        plt.legend(handles=legend_handles, loc=legend_location)
    else:
        if plot_energy:
            energy_axis.legend(loc=legend_location)
        if plot_time:
            time_axis.legend(loc=legend_location)


def plot_cpu_frequency_test_results(output_dir="./"):
    for function, kwargs, filename in [
        (plot_experiment_1_comparison, {}, "experiment_1_comparison.svg",),
        (
            plot_cpu_time_and_energy,
            {
                "csv_file_path": "test_results_2.csv",
                "use_twinx": False,
                "plot_type": "scatter",
                "plot_average": True,
                "average_same_color": True,
                "per_short_read": False,
            },
            "experiment_2.svg",
        ),
    ]:
        function(**kwargs)
        plt.savefig(os.path.join(output_dir, filename))
        plt.clf()


if __name__ == "__main__":
    plac.call(plot_cpu_frequency_test_results)
