#!/usr/bin/python3

import inspect
import os
import subprocess
import time
import plac
import matplotlib.pyplot as plt
import numpy as np

def plot_unrelated_supply_voltage():
    x_max = 2
    y_max = 5
    V_supply = np.linspace(0, x_max, 1000)
    f = np.linspace(0, x_max, 1000)
    E_total_V_supply = np.exp(-1) * V_supply + V_supply**2
    E_total_f = np.exp(-1)/f + f
    E_total_sum = E_total_V_supply + E_total_f
    _, (V_supply_axis, f_axis) = plt.subplots(nrows=2, ncols=1, figsize=(6, 8))
    V_supply_axis.set_xlabel("V_supply")
    f_axis.set_xlabel("f")
    V_supply_axis.set_ylabel("E_total")
    f_axis.set_ylabel("E_total")
    V_supply_axis.plot(V_supply, E_total_V_supply, label="E_total(V_supply)", color="tab:blue")
    f_axis.plot(V_supply, E_total_f, label="E_total(f)", color="tab:red")
    # plt.plot(V_supply, E_total_sum, label="Sum")
    V_supply_axis.set_ylim(0, y_max)
    f_axis.set_ylim(0, y_max)
    # V_supply_axis.plot([1, 1], [0, y_max], linestyle="dashed", label="V_threshold", color="tab:red")
    V_supply_axis.legend()
    f_axis.legend()

def plot_exponential_supply_voltage():
    V_supply = np.linspace(1, 4, 1000)
    E_total = (1 + np.exp(1)/(V_supply - 1)**1.3) * V_supply**2
    f_min = np.argmin(E_total)
    plt.xlabel("V_supply")
    plt.ylabel("E_total")
    plt.plot(V_supply, E_total, label="E_total")
    y_max = 40
    plt.ylim(0, y_max)
    plt.plot([1, 1], [0, y_max], linestyle="dashed", label="V_threshold", color="tab:red")
    plt.plot(V_supply[f_min], E_total[f_min], lw=0, marker="o", label="f_min", color="tab:purple")
    plt.legend()

def plot_linear_supply_voltage():
    V_supply = np.linspace(0, 5, 1000)
    E_total = np.exp(-1) + V_supply**2
    plt.xlabel("V_supply")
    plt.ylabel("E_total")
    plt.plot(V_supply, E_total, label="E_total")
    plt.ylim(0, 20)
    plt.plot([1, 1], [0, 20], linestyle="dashed", label="V_threshold")
    plt.legend()

def plot_dvfs_theory(output_dir="./"):
    for function, filename in [
        (plot_unrelated_supply_voltage, "unrelated_supply_voltage.svg",),
        (plot_exponential_supply_voltage, "exponential_supply_voltage.svg",),
        (plot_linear_supply_voltage, "linear_supply_voltage.svg",),
    ]:
        plt.figure(figsize=(6, 4))
        function()
        plt.savefig(os.path.join(output_dir, filename))
        plt.clf()


if __name__ == "__main__":
    plac.call(plot_dvfs_theory)
