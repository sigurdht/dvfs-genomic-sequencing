#!/bin/sh
set -e
cd $(dirname $0)/

# This script is a workaround for pushing  `dvfs-genomic-sequencing-swaram_dvfs_odroid`, since I can't push directly to IDI's gitlab from the Odroid, since I'm not able to install AnyConnect VPN on the Odroid.

sshpass -p odroid ssh odroid@odroid tar -czf swaram_dvfs_odroid.tar.gz swaram_dvfs_odroid
sshpass -p odroid scp odroid@odroid:~/swaram_dvfs_odroid.tar.gz ./
sshpass -p odroid ssh odroid@odroid rm swaram_dvfs_odroid.tar.gz
rm -rf swaram_dvfs_odroid
tar -xzf swaram_dvfs_odroid.tar.gz
rm swaram_dvfs_odroid.tar.gz
cd swaram_dvfs_odroid/
    git push -u origin --all
    git push -u origin --tags
cd ../
