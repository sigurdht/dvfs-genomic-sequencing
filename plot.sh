#!/bin/sh
# set -e
cd $(dirname $0)/

rm -rf plots/*
python source_code/plot/plot.py plots
