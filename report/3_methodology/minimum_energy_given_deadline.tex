\section{DVFS minimum energy given deadline}
\label{sec:minimum_energy_given_deadline}

In this section we will describe an algorithm for determining the optimal frequency of a CPU for completing a process before a given deadline. The optimal frequency is defined as the frequency that makes the entire system use as little energy as possible. But there is a constraint that the process must finish before the deadline. Or at least be expected to finish before the deadline. Mathematically, this can be written as

\begin{equation}
    \label{eq:optimal_cpu_frequency_given_deadline}
    \begin{aligned}
        \fOptimal &= \argmin_f \Eexpected(f),\\
        \TexpectedLocal(\fOptimal) &\leq \Tdeadline,
    \end{aligned}
\end{equation}

where $f$ is CPU frequency, $\Eexpected$ is the expected total energy consumption as a function of CPU frequency, $\TexpectedLocal$ is the expected runtime as a function of CPU frequency and $\Tdeadline$ is the deadline. To define an algorithm that finds this frequency, we first of all need to specify how we calculate $\Eexpected(f)$ and $\TexpectedLocal(f)$. For this, the algorithm will be based on experimentally found reference values for these properties. We will come back to how we obtain these reference values later in the report. We assume the CPU has a finite number of clock frequencies at which it can operate. Then the inputs to the algorithm will be:

\begin{itemize}
    \item $\fAvailableVec$, a list of which frequencies the CPU can operate at.
    \item $\EreferenceLocalVec$, a list of experimentally found total energy consumption values for the process. Each item corresponds to one of the available frequencies.
    \item $\TreferenceLocalVec$, a list of experimentally found total time usage values for the process. Each item corresponds to one of the available frequencies.
    \item $\TestimateLocal$, the estimated total time usage for the process that is specific for the current input to the process if the CPU is operated at its maximum frequency.
    \item $\Tdeadline$, the maximum total time the process should take.
\end{itemize}

Pseudo-code for the algorithm is shown in Algorithm \ref{alg:get_optimal_cpu_frequency}. The algorithm starts by defining the variable $\Wrelative$ as the ratio between the estimated total time and the reference total time at the highest CPU frequency:

\begin{equation}
    \label{eq:wrelative}
    \Wrelative = \TestimateLocal / \max_f \TreferenceLocalVec(f).
\end{equation}

Since higher CPU frequency always corresponds to lower (or equal) processing time, the last part is equivalent to taking the minimum value of $\TreferenceLocalVec$:

\begin{equation}
    \Wrelative = \TestimateLocal / \min \TreferenceLocalVec.
\end{equation}

Then we iterate over all the available frequencies to find which one has the lowest reference energy consumption while also being expected to meet the deadline. To check if a frequency is expected to meet the deadline, we check if the product of the frequency's reference time usage, $\TreferenceLocalVec(f)$ and $\Wrelative$ are less than or equal to $\Tdeadline$. In other words, we use this product as our estimator of total time usage if the CPU is operated at $f$:

\begin{equation}
    \label{eq:texpected}
    \TexpectedLocal(f) = \TreferenceLocalVec(f) \cdot \Wrelative.
\end{equation}

That means our estimator assumes there is a linear (and proportional) mapping across all CPU frequencies from the reference time usage to the time usage of the process with its current input. This proportionality constant is $\Wrelative$. This estimator was chosen of multiple reasons. First and foremost, it is very flexible, it sets no strict limitations to the exact values of $\EreferenceLocalVec$ and $\TreferenceLocalVec$. That means they can either be provided per short read, per base pair, or as direct results from a larger experimental run from a realistic problem size. It even makes the algorithm applicable to any type of computation process. The only important thing is that $\EreferenceLocalVec$ and $\TreferenceLocalVec$ have the same proportion for all frequencies. Their only role is to provide a relative relationship between each CPU frequency in terms of energy consumption and time usage of some reference process input. Secondly, it requires little knowledge about how the current input affects energy and runtime at different frequencies. It only needs an estimate for one frequency, in particular, the maximum frequency.

\begin{algorithm}
    \caption{Get optimal CPU frequency given deadline.}
    \label{alg:get_optimal_cpu_frequency}
    \small
    \begin{algorithmic}
        \Procedure{GetOptimalCpuFrequencyGivenDeadline}{$\fAvailableVec$, $\EreferenceLocalVec$, $\TreferenceLocalVec$,\wrapline
            $\TestimateLocal$, $\Tdeadline$}{}
            \State $\Wrelative \gets \TestimateLocal / \min \TreferenceLocalVec$
            \State $\fOptimal \gets \max \fAvailableVec$
            \For{$f \in \fAvailableVec$}
                \State $\TexpectedLocal \gets \TreferenceLocalVec(f) \cdot \Wrelative$
                \If{$\TexpectedLocal \leq \Tdeadline \AND \EreferenceLocalVec(f) < \EreferenceLocalVec(\fOptimal)$}
                    \State $\fOptimal \gets f$
                \EndIf
            \EndFor
            \State \Return{$\fOptimal$}
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

\subsection{Multiple Voltage-Frequency Islands (VFIs)}

The algorithm as described above is only capable of optimizing one CPU frequency on the entire target node. If the target node has multiple VFIs, there is not just one frequency value to optimize, but rather a vector of frequencies, one value for each independently controllable VFI:

\begin{equation}
    \label{eq:node_cpu_frequencies}
    \fVector =
    \begin{pmatrix}
        \fCpu{0}\\
        \vdots\\
        \fCpu{\numvfis-1}
    \end{pmatrix}.
\end{equation}

There are many ways to adapt Algorithm \ref{alg:get_optimal_cpu_frequency} to work with multiple VFIs. For example, the algorithm could accept a matrix or hyper-dimensional tensor of available frequency combinations with corresponding reference energy and runtime values, reflecting all the possible combinations of frequencies for each VFI. This can work for a few VFIs, but does not scale. A more scalable solution can instead accept reference values isolated for each VFI, and optimize them in turn.
% To define an algorithm that works just like Algorithm \ref{alg:get_optimal_cpu_frequency}, but that can be used for optimizing multiple VFIs, we need to provide available combinations of frequencies instead of $\fAvailableVec$. Similarly, $\EreferenceLocalVec$ and
% we need an algorithm that does the exact same thing, but instead of just containing all configurable frequencies for a VFI, the vectors $\fAvailableVec$, $\EreferenceLocalVec$ and $\TreferenceLocalVec$ will have to contain all possible combinations of frequencies across all VFIs. In the case of 2 VFIs, that would mean an $N \times M$ matrix, where $N$ and $M$ are the number of available frequencies for each VFI. And in the case of 3 VFIs, we would get a 3-dimensional tensor! In other words, we would have to do exponentially many more experiments to get all the values required for $\EreferenceLocalVec$ and $\TreferenceLocalVec$.

% One way of mitigating this issue is to assume a linear model of how each VFIs frequency affects energy and runtime, so that you perform experiments of the impacts of changing the frequency of each VFI individually, and fill out the rest of the matrix or tensor by inferring the results with this model. This probably work ok for 2-4 VFIs with a small number of available frequencies each. But for even more VFIs a more scalable algorithm quickly becomes a necessity.

However, the simplest thing to do is to stick with Algorithm \ref{alg:get_optimal_cpu_frequency} as is and dynamically adjust the frequency of only one VFI. All other VFIs will be set to operate at a constant frequency that is shown to have a suitable runtime/energy tradeoff for the application of interest. Depending on the characteristics of the energy/runtime for the process as function of the frequencies of the VFIs, it might very well give good enough results to exploit most of the DVFS optimization potential. In fact, we will see in the results (Section \ref{sec:experiment_1}) that this is the case for BWA-MEM on the Odroid-XU4, so in this report we will go further with this approach.
