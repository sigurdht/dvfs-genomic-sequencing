\section{Configure CPU frequency on Odroid-XU4}
\label{sec:configure_cpu_frequency}

One of the most basic tasks to solve to be able to do DVFS on SWARAM is to be able to configure the clock frequencies of the different CPU cores of the Odroid-XU4. CPU clock frequency configurations on the Odroid-XU4 can be read and set by the programs \verb|cpufreq-info| and \verb|cpufreq-set| respectively. We developed 3 thin Python wrappers around these programs for easier access in other Python scripts. These wrappers are located in the Python file \verb|cpu_frequency.py|, listed in Appendix \ref{sec:python_code:cpu_frequency}.

The first wrapper function is \verb|get_cpu_frequency(cpu_num)| around \verb|cpufreq-info|. We use the \verb|-f| flag to get the only the frequency as an integer and the \verb|-c <cpu_num>| flag to select for which CPU. Note that we have to provide the CPU number as listed by the OS, even though 0-3 always will have the same value and 4-7 always will have the same value. On the python-technical side, the command is executed as a subprocess, and we use the \verb|subprocess.check_output| function since we want to read the output from the command. The \verb|shell=True| is necessary for telling Python that we want to run the command as in a shell. If not we would have to manually split the command into an argument list, and that would make the code a lot less readable.

\lstset{style=pythonstyle}
\begin{lstlisting}[language=Python3]
def get_cpu_frequency(cpu_num):
    return int(subprocess.check_output(f"cpufreq-info -fc {cpu_num}", shell=True).decode("utf-8"))
\end{lstlisting}

The second wrapper function is \verb|set_cpu_frequency(cpu_num, cpu_frequency)| around \verb|cpufreq-set|. We use the \verb|-c <cpu_num>| flag to select CPU and \verb|-f <frequency_khz>| to select frequency. Only a single CPU number should be given, \verb|cpufreq-set| sets the provided frequency on the entire VFI when only one of the CPU cores in the VFI is given. Again, we use a Python subprocess, but this time we use the function \verb|subprocess.call|, which does not return the process output. To check if the operation succeeded the most robust method is anyways to call \verb|get_cpu_frequency| and check if the frequency is correct.

\lstset{style=pythonstyle}
\begin{lstlisting}[language=Python3]
def set_cpu_frequency(cpu_num, cpu_frequency):
    subprocess.call(f"sudo cpufreq-set -c {cpu_num} -f {cpu_frequency:.0f}", shell=True)
\end{lstlisting}

In addition, \verb|cpufreq-set| on Odroid-XU4 supports configuring CPU frequency governors with the \verb|-g <governor>| flag. Setting a CPU frequency governor tells the OS to let a the governor automatically adjust the CPU frequency. You can choose between different governors for different workloads and performance requirements. Some examples of governors are ''idle'', ''ondemand'' and ''performance''. This gives us the last wrapper function, \verb|set_cpu_governor(cpu_num, governor)|.

\lstset{style=pythonstyle}
\begin{lstlisting}[language=Python3]
def set_cpu_governor(cpu_num, governor):
    subprocess.call(f"sudo cpufreq-set -c {cpu_num} -g {governor}", shell=True)
\end{lstlisting}

To test out these functions and programs, we first created a configurable Python test script. This script is located in \verb|test_cpu_frequencies.py|, listed in Appendix \ref{sec:python_code:test_cpu_frequencies}. The script takes two flags \verb|--cpu-0-3 <frequency_khz>| and \verb|--cpu-4-7 <frequency_khz>|. If any of the flags are omitted, the script will not use this CPU. This interface was selected because it is intuitive to understand and easily configurable. The script is useful to verify that the DVFS management works in a situation where you don't have any useful intensive work ready to perform. It is very simple to run, the \verb|heavy_operation()| function takes no inputs and gives no outputs. After executing the script with high frequencies on both VFIs you should quickly hear the fan speeds up, and when you try with lower frequencies, both the script and the fan will run noticeably slower if everything works as it should.
