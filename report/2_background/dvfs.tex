\section{Dynamic Voltage- and Frequency Scaling (DVFS)}
\label{sec:dvfs}

Dynamic Voltage- and Frequency Scaling, or DVFS for short, is a a power saving technique that can be applied to all digital systems, including complex computer systems running on large or even multiple SoCs.

The dynamic power consumption of a CMOS transistor in a digital system is given by

\begin{equation}
    \label{eq:dynamic_power}
    \Pdynamic = \Idynamic \Vsupply = \alpha f C \Vsupply^2.
\end{equation}

$\alpha$ is the activity factor, or the percentage of the clock edges at which that particular transistor switches. $C$ is the load capacitance. Intuitively, this increases linearly with respect to $f$. The static power consumption is given by

\begin{equation}
    \label{eq:static_power}
    \Pstatic = \Ileakage \Vsupply.
\end{equation}

In traditional DVFS analysis, in the earlier phase of Moore's law, dynamic power was assumed to dominate static power. But due to short-channel effects in small technology nodes, this is simply not true for modern SoCs. This is because short-channel effects cause an exponential increase in leakage current for decreasing gate length \cite{kim2003leakagecurrent}. A simple model for leakage current is given by

\begin{equation}
    \label{eq:simple_leakage_current}
    \Ileakage = \gamma e^{-\Vthreshold}
\end{equation}

\cite{dally2016dads1}, which gives

\begin{equation}
    \label{eq:static_power_leakage_current}
    \Pstatic = \gamma e^{-\Vthreshold} \Vsupply.
\end{equation}

The total power consumption is the sum of static and dynamic, and we get

\begin{equation}
    \begin{aligned}
        \Ptotal & = \Pstatic + \Pdynamic\\
                & = \gamma e^{-\Vthreshold} \Vsupply + \alpha f C \Vsupply^2.
    \end{aligned}
\end{equation}

The preceding equation is the most interesting for digital designers when performance is limited by heat dissipation. But for the purpose of this report, we are interested in how much energy is consumed to perform a given workload. This total energy is given by $\Ptotal \cdot t$, where  $t$ is the time taken to perform the workload. If we are able to completely power off the system when the workload is done, this time is equal to the clock period times the number of clock cycles $N$ that passed through the entire workload, and we can express the total energy consumption as

\begin{equation}
    \label{eq:total_energy}
    \begin{aligned}
        \Etotal & = \Ptotal \frac{N}{f}\\
                & = \gamma e^{-\Vthreshold} \frac{N \Vsupply}{f} + \alpha N C \Vsupply^2\\
                & \propto \frac{\Vsupply}{f} + f \Vsupply^2.
    \end{aligned}
\end{equation}

If we take a quick look at how this develops for $f$ and $\Vsupply$ independently, we see that for large values of $\Vsupply$, it's quadratic term, which came from the dynamic power will dominate the total energy. And for small values of $f$, its inverse proportional term, which came from the static power will dominate the total energy.

\begin{figure}
    \centering
    \includesvg[width=.8\textwidth]{figures/plots/unrelated_supply_voltage.svg}
    \caption{Plot of $\Etotal$, seeing $f$ and $\Vsupply$ as unrelated variables. All parameters are set to 1.}
\end{figure}

But when we do DVFS, we always want to set $\Vsupply$ as low as possible for the given $f$. These variables are in other words highly dependent. To be able to really discuss how the total energy is related to $f$, we need to define how $f$ is related to $\Vsupply$.

Obviously, we can assume that there is a strictly increasing relationship between $f$ and $\Vsupply$, if not we could simply lower $\Vsupply$ and increase $f$ at the same time, and we would be able to get infinitely high performance for an infinitesimally small $\Vsupply$, and all our problems regarding power consumption in computers would be solved, and I would not sit here and write this report. To start with the simplest imaginable relationship, we will now assume that there is a positive linear relationship between $f$ and $\Vsupply$:

\begin{equation}
    \label{eq:linear_voltage_frequency}
    f = \beta \Vsupply.
\end{equation}

% \begin{equation}
%     \Ptotal = \gamma e^{-\Vthreshold} \Vsupply + \alpha \beta C \Vsupply^3 \propto \Vsupply \left(1 + \Vsupply^2\right)
% \end{equation}

By inserting this into (\ref{eq:total_energy}), we get

\begin{equation}
    \begin{aligned}
        \Etotal & = \frac{\gamma N}{\beta} e^{-\Vthreshold} + \alpha N C \Vsupply^2\\
                & \propto 1 + \Vsupply^2.
    \end{aligned}
\end{equation}

In this case, we will always save total energy by reducing $\Vsupply$. In practice, this is not a good enough assumption to study small values of $\Vsupply$. According to \cite{kim2003leakagecurrent}, a better estimation is given by

\begin{equation}
    \label{eq:exponential_voltage_frequency}
    f = \delta \frac{(\Vsupply - \Vthreshold)^\epsilon}{\Vsupply},
\end{equation}

where $\epsilon$ is a technology dependent parameter, currently equal to 1.3. By instead inserting this into (\ref{eq:total_energy}), we get

\begin{equation}
    \begin{aligned}
        \Etotal & = \frac{\gamma N}{\delta} e^{-\Vthreshold} \frac{\Vsupply^2}{(\Vsupply - \Vthreshold)^{1.3}} + \alpha N C \Vsupply^2\\
                & \propto \left(1 + \frac{1}{(\Vsupply - \Vthreshold)^{1.3}}\right) \Vsupply^2
    \end{aligned}
\end{equation}

Now, if we assume a large $\Vsupply$ relative to $\Vthreshold$, the $\Vsupply^2$ term will dominate, but when $\Vsupply$ get's smaller and relatively close to $\Vthreshold$, then the expression inside the parenthesis will dominate. This means that there is a point where we cannot reduce $\Vsupply$ further without increasing total energy. This behavior is seen in Figure \ref{fig:exponential_leakage_current}.

\begin{figure}
    \centering
    \includesvg[width=.8\textwidth]{figures/plots/exponential_supply_voltage.svg}
    \caption{Plot of $\Etotal$ with all parameters set to 1, including $\Vthreshold$.}
    \label{fig:exponential_leakage_current}
\end{figure}

An important property of this model is that $\Etotal(f)$ has a single global minimum called $\fMin$ and is strictly growing in both directions away from it. Note that the graph for $\Etotal(\Vsupply)$ in Figure \ref{fig:exponential_leakage_current} is not exactly representative for $\Etotal(f)$, since it is a function of $\Vsupply$, but our model assumes a strictly growing relationship between $f$ and $\Vsupply$ so there will still be a single global minimum $\fMin$ that sense, common for $\Etotal(f)$ and $\Etotal(\Vsupply)$. In this way we can assume that as long as $f > \fMin$, a lower value of $f$ is always equivalent to a lower value of $\Etotal(f)$. In other words, we save energy by reducing frequency.
